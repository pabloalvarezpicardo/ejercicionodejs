var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  path = require('path');


app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res){
  res.sendFile(path.join(__dirname,'index.html'));
});

app.post('/', function(req, res){
  res.send('Hemos recibido su peticion cambiada!');
});

app.get('/clientes/:idcliente', function(req, res){
  res.send('Aqui tiene al cliente numero ' + req.params.idcliente);
});

app.get('/clientes', function(req, res){
  res.sendFile(path.join(__dirname,'lectura.json'));
});
app.post('/clientes', function(req, res){
  var resp = {
    "mensaje": "Hemos dado de alta su cliente"
  };
  res.send(resp);
});
app.put('/clientes', function(req, res){
  var resp = {
    mensaje: 'Hemos modificado su cliente'
  };
  res.send(resp);
});
app.delete('/clientes', function(req, res){
  var resp = {
    mensaje: 'Hemos eliminado su cliente'
  };
  res.send(resp);
});
